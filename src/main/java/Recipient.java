import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Recipient extends Client{

    /**
     * Класс получателя
     */


    public Recipient(String name, String lastname, String patronymic, String cardNumber, String accountNumber, Map<Currency, BigDecimal> wallet) {
        super(name, lastname, patronymic, cardNumber, accountNumber, wallet);
    }


}
