import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class AppMain {

    /**
     * 1. Создаются средства для Ивана Ивановича Иванова, создается обьект "Отправитель"
     * 2. Создаются средства для Михаила Васильевича Сидоров, создается обьект "Получатель"
     * 3. Создается обьект Date, для фиксировния времени перевода
     * 4. Происходит перевод с карты на карту в Долларах
     * 5. Вывод перевода со счета на счет
     * 6. Вывода перевода с карты на карту
     * 7. Демонстрирование методов верхнего и нижнего регистра
     * 8. Демонстрирование метода Json Обьектов
     */

    public static void main(String[] args) {

        Map<Currency, BigDecimal> ivanWallet = new HashMap<>();

        ivanWallet.put(Currency.USD, new BigDecimal(50));
        ivanWallet.put(Currency.EUR, new BigDecimal(30));
        ivanWallet.put(Currency.RUB, new BigDecimal(1550));


        Client ivan_Ivanovich_Ivanov = new Sender("Иван",
                "Иванов",
                "Иванович",
                "1234",
                "5678", ivanWallet);

        Map<Currency, BigDecimal> mikhailWallet = new HashMap<>();
        mikhailWallet.put(Currency.USD, new BigDecimal(10));
        mikhailWallet.put(Currency.EUR, new BigDecimal(20));
        mikhailWallet.put(Currency.RUB, new BigDecimal(2050));

        Client mikhail_Vasilyevich_Sidorov = new Recipient("Михаил",
                "Сидоров",
                "Васильевич",
                "9101",
                "1213", mikhailWallet);


        Transfer transferAccountToAccount = new TransferAccountToAccount(new BigDecimal(5),Currency.USD,
                ivan_Ivanovich_Ivanov, mikhail_Vasilyevich_Sidorov);

        Transfer transferCardToCard = new TransferCardToCard(new BigDecimal(2),Currency.EUR,
        ivan_Ivanovich_Ivanov, mikhail_Vasilyevich_Sidorov);

        transferAccountToAccount.doTransfer();
        transferAccountToAccount.doTransfer();
        transferCardToCard.doTransfer();

        System.out.println(ivan_Ivanovich_Ivanov.getNameUpperCase());
        System.out.println(ivan_Ivanovich_Ivanov.getNameLowerCase()+"\n");

        System.out.println(transferAccountToAccount.getJsonString(transferAccountToAccount));
        System.out.println(transferCardToCard.getJsonString(transferCardToCard));



    }
}
