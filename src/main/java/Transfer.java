import com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public abstract class Transfer {

    /**
     * Абстрактный класс для перевода средств
     */

    private static ObjectMapper mapper = new ObjectMapper();

    protected int transferNumber;
    protected BigDecimal transferAmount;
    protected Currency currency;
    protected Client recipient;
    protected Client sender;
    protected Date transferDate;
    private static int transferCounter = 0;

    /**
     *
     * @param transferAmount - Количество отправленной валюты
     * @param currency - Валюта для перевода
     * @param sender - Отправитель
     * @param recipient - Получатель
     */
    public Transfer(BigDecimal transferAmount, Currency currency, Client sender, Client recipient) {
        this.transferAmount = transferAmount;
        this.currency = currency;
        this.sender = sender;
        this.recipient = recipient;
        transferDate = new Date();
    }

    /**
     * Метод проверяет в условие количество средств у отправителя через метод {@link Client#checkBalance(Currency, BigDecimal)}
     * если условие выполняется то у отправителя уменьшается количество определнной валюты, а у получаетеля увеличивается,
     * если условие не выполнилось выводится соотвествующее сообщение
     */
    public void doTransfer(){
        transferNumber = transferCounter++;
        if(sender.checkBalance(currency, transferAmount)){
            recipient.wallet.put(currency, recipient.wallet.get(currency).subtract(transferAmount));
            sender.wallet.put(currency, sender.wallet.get(currency).subtract(transferAmount));
            System.out.println(toString());
        } else {
            System.out.println("Перевод совершить нельзя, недостаточно средств");
        }
    }

    /**
     * Метод возвращает результат перевода в виде Json обьекта
     * @return JSON обьект в виде String
     */
    public String getJsonString(Transfer transfer){

        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(transfer);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return jsonString;
    }

    @Override
    public String toString() {
        return "Номер перевода: "+transferNumber+
                ", Дата: "+transferDate+
                ", Валюта: "+currency+
                ", Сумма: "+transferAmount+
                ", Получатель: "+sender.lastname +" "+sender.name+" "+sender.patronymic+
                ", Отправитель: "+recipient.lastname +" "+recipient.name+" "+recipient.patronymic+"\n";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return transferNumber == transfer.transferNumber && Objects.equals(transferAmount, transfer.transferAmount) && currency == transfer.currency && Objects.equals(recipient, transfer.recipient) && Objects.equals(sender, transfer.sender) && Objects.equals(transferDate, transfer.transferDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transferNumber, transferAmount, currency, recipient, sender, transferDate);
    }

    public int getTransferNumber() {
        return transferNumber;
    }

    public BigDecimal getTransferAmount() {
        return transferAmount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Client getRecipient() {
        return recipient;
    }

    public Client getSender() {
        return sender;
    }

    public Date getTransferDate() {
        return transferDate;
    }
}
