import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public abstract class Client {

    /**
     * Абстрактный класс клиента
     */

    String name;
    String lastname;
    String patronymic;
    String cardNumber;
    String accountNumber;
    Map<Currency, BigDecimal> wallet;

    /**
     *
     * @param name - Имя
     * @param lastname - Фамилия
     * @param patronymic - Отчество
     * @param cardNumber - Номер карты
     * @param accountNumber - Номер счета

     */
    public Client(String name, String lastname, String patronymic, String cardNumber, String accountNumber, Map<Currency, BigDecimal> wallet) {
        this.name = name;
        this.lastname = lastname;
        this.patronymic = patronymic;
        this.cardNumber = cardNumber;
        this.accountNumber = accountNumber;
        this.wallet = wallet;
    }

    /**
     * Метод возвращает true или false в зависимости от наличие определенной валюты
     * @param currency - валюта для перевода
     * @param currencyAmount - количество данной валюты для перевода
     * @return Внешнее условие проверяет наличие определенной валюты.
     * Вложенное условие возвращяет true, если на счете отправителя хватает средств для перевода
     * и возвращает false если на счете отправителя недостаточно средств.
     */
    public boolean checkBalance(Currency currency, BigDecimal currencyAmount){

        if(wallet.containsKey(currency)){
            return wallet.get(currency).compareTo(currencyAmount) > 0;
        }
        System.out.println("Данной валюты не сущесвует");
        return false;

    }

    /**
     * Метод для возвращение Фамилии, Имени и Отчества (ФИО)
     * @return String фио
     */
    private String getFullName(){
        return lastname+" "+name+" "+patronymic;
    }

    /**
     * Метод для возвращение Фамилии, Имени и Отчества (ФИО) в верхнем регистре
     * @return String ФИО
     */
    public String getNameUpperCase(){
        return getFullName().toUpperCase();
    }

    /**
     * Метод для возвращение Фамилии, Имени и Отчества (ФИО) в нижнем регистре
     * @return String фио
     */
    public String getNameLowerCase(){
        return getFullName().toLowerCase();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(name, client.name) && Objects.equals(lastname, client.lastname) && Objects.equals(patronymic, client.patronymic) && Objects.equals(cardNumber, client.cardNumber) && Objects.equals(accountNumber, client.accountNumber) && Objects.equals(wallet, client.wallet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastname, patronymic, cardNumber, accountNumber, wallet);
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}
