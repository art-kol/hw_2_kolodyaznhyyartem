import java.math.BigDecimal;
import java.util.Date;

public class TransferCardToCard extends Transfer{

    /**
     * Класс для перевода с карты на карту
     */

    public TransferCardToCard(BigDecimal transferAmount, Currency currency, Client recipient, Client sender) {
        super(transferAmount, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return super.toString()+
                "Номер счета получателя: "+ sender.cardNumber+
                ", Номер счета отправителя: "+recipient.cardNumber+"\n";
    }

}
