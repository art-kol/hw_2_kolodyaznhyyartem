import java.math.BigDecimal;
import java.util.Date;

//import java.util.Date;
//
public class TransferAccountToAccount extends Transfer{

    /**
     * Класс для перевода со счета на счет
     */

    public TransferAccountToAccount(BigDecimal transferAmount, Currency currency, Client recipient, Client sender) {
        super(transferAmount, currency, recipient, sender);
    }

    @Override
    public String toString() {
        return super.toString()+
                "Номер счета получателя: "+ sender.accountNumber+
                ", Номер счета отправителя: "+recipient.accountNumber+"\n";
    }

}
