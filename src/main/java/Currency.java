/**
 * Перечисление валют
 */
public enum Currency {
    USD,
    EUR,
    RUB,
}
